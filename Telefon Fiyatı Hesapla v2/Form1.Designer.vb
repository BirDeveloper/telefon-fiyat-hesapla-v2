﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Hesapla = New System.Windows.Forms.Button()
        Me.Kur = New System.Windows.Forms.NumericUpDown()
        Me.Fiyat = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.VergisizFiyat = New System.Windows.Forms.Label()
        Me.VergisizFiyatDeğer = New System.Windows.Forms.Label()
        Me.TrtPayı = New System.Windows.Forms.Label()
        Me.TrtPayıDeğer = New System.Windows.Forms.Label()
        Me.ÖtvPayı = New System.Windows.Forms.Label()
        Me.ÖtvPayıDeğer = New System.Windows.Forms.Label()
        Me.KdvPayı = New System.Windows.Forms.Label()
        Me.KdvPayıDeğer = New System.Windows.Forms.Label()
        Me.KültürPayı = New System.Windows.Forms.Label()
        Me.KültürPayıDeğer = New System.Windows.Forms.Label()
        Me.SatışFiyatıDeğer = New System.Windows.Forms.Label()
        Me.SatışFiyatı = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.HesapTürü = New System.Windows.Forms.ComboBox()
        Me.ParaBirimi = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.KayıtsızPanel = New System.Windows.Forms.Panel()
        Me.ToplamMaliyet = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.KayıtÜcretiDeğer = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TrtPayıDeğer2 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.VergisizFiyatDeğer2 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.KurYenile = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        CType(Me.Kur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.KayıtsızPanel.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KurYenile, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label3.Location = New System.Drawing.Point(26, 205)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(119, 18)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Teknoseyir / Temp"
        '
        'Hesapla
        '
        Me.Hesapla.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Hesapla.Location = New System.Drawing.Point(36, 173)
        Me.Hesapla.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.Hesapla.Name = "Hesapla"
        Me.Hesapla.Size = New System.Drawing.Size(93, 27)
        Me.Hesapla.TabIndex = 16
        Me.Hesapla.Text = "Hesapla"
        Me.Hesapla.UseVisualStyleBackColor = True
        '
        'Kur
        '
        Me.Kur.DecimalPlaces = 2
        Me.Kur.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Kur.Increment = New Decimal(New Integer() {10, 0, 0, 131072})
        Me.Kur.Location = New System.Drawing.Point(26, 19)
        Me.Kur.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.Kur.Name = "Kur"
        Me.Kur.Size = New System.Drawing.Size(114, 26)
        Me.Kur.TabIndex = 13
        Me.Kur.Value = New Decimal(New Integer() {690, 0, 0, 131072})
        '
        'Fiyat
        '
        Me.Fiyat.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Fiyat.Location = New System.Drawing.Point(26, 145)
        Me.Fiyat.Margin = New System.Windows.Forms.Padding(2, 4, 2, 4)
        Me.Fiyat.MaxLength = 13
        Me.Fiyat.Name = "Fiyat"
        Me.Fiyat.Size = New System.Drawing.Size(115, 23)
        Me.Fiyat.TabIndex = 12
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label2.Location = New System.Drawing.Point(26, 127)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 18)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Telefon Fiyatı"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label1.Location = New System.Drawing.Point(26, 1)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 18)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Dolar Kuru"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'VergisizFiyat
        '
        Me.VergisizFiyat.AutoSize = True
        Me.VergisizFiyat.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.VergisizFiyat.Location = New System.Drawing.Point(152, 2)
        Me.VergisizFiyat.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.VergisizFiyat.Name = "VergisizFiyat"
        Me.VergisizFiyat.Size = New System.Drawing.Size(86, 15)
        Me.VergisizFiyat.TabIndex = 19
        Me.VergisizFiyat.Text = "Vergisiz Fiyat:"
        Me.VergisizFiyat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.VergisizFiyat, "Vergisiz Fiyat, Vergilerin Dahil Olmadığı Ham Fiyattır.")
        '
        'VergisizFiyatDeğer
        '
        Me.VergisizFiyatDeğer.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.VergisizFiyatDeğer.ForeColor = System.Drawing.Color.Red
        Me.VergisizFiyatDeğer.Location = New System.Drawing.Point(156, 17)
        Me.VergisizFiyatDeğer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.VergisizFiyatDeğer.Name = "VergisizFiyatDeğer"
        Me.VergisizFiyatDeğer.Size = New System.Drawing.Size(78, 21)
        Me.VergisizFiyatDeğer.TabIndex = 20
        Me.VergisizFiyatDeğer.Text = "0"
        Me.VergisizFiyatDeğer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TrtPayı
        '
        Me.TrtPayı.AutoSize = True
        Me.TrtPayı.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.TrtPayı.Location = New System.Drawing.Point(165, 74)
        Me.TrtPayı.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TrtPayı.Name = "TrtPayı"
        Me.TrtPayı.Size = New System.Drawing.Size(60, 15)
        Me.TrtPayı.TabIndex = 21
        Me.TrtPayı.Text = "TRT Payı:"
        Me.TrtPayı.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.TrtPayı, "TRT Payı, %10 Olarak Hesaplanmaktadır.")
        '
        'TrtPayıDeğer
        '
        Me.TrtPayıDeğer.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.TrtPayıDeğer.ForeColor = System.Drawing.Color.Red
        Me.TrtPayıDeğer.Location = New System.Drawing.Point(156, 89)
        Me.TrtPayıDeğer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TrtPayıDeğer.Name = "TrtPayıDeğer"
        Me.TrtPayıDeğer.Size = New System.Drawing.Size(78, 21)
        Me.TrtPayıDeğer.TabIndex = 22
        Me.TrtPayıDeğer.Text = "0"
        Me.TrtPayıDeğer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ÖtvPayı
        '
        Me.ÖtvPayı.AutoSize = True
        Me.ÖtvPayı.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.ÖtvPayı.Location = New System.Drawing.Point(164, 110)
        Me.ÖtvPayı.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.ÖtvPayı.Name = "ÖtvPayı"
        Me.ÖtvPayı.Size = New System.Drawing.Size(62, 15)
        Me.ÖtvPayı.TabIndex = 23
        Me.ÖtvPayı.Text = "ÖTV Payı:"
        Me.ÖtvPayı.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.ÖtvPayı, "ÖTV Payı, Tutara Göre Değişen %25/%40/%50 Gibi Oranlarda Hesaplanmaktadır.")
        '
        'ÖtvPayıDeğer
        '
        Me.ÖtvPayıDeğer.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.ÖtvPayıDeğer.ForeColor = System.Drawing.Color.Red
        Me.ÖtvPayıDeğer.Location = New System.Drawing.Point(156, 125)
        Me.ÖtvPayıDeğer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.ÖtvPayıDeğer.Name = "ÖtvPayıDeğer"
        Me.ÖtvPayıDeğer.Size = New System.Drawing.Size(78, 21)
        Me.ÖtvPayıDeğer.TabIndex = 24
        Me.ÖtvPayıDeğer.Text = "0"
        Me.ÖtvPayıDeğer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'KdvPayı
        '
        Me.KdvPayı.AutoSize = True
        Me.KdvPayı.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.KdvPayı.Location = New System.Drawing.Point(164, 146)
        Me.KdvPayı.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.KdvPayı.Name = "KdvPayı"
        Me.KdvPayı.Size = New System.Drawing.Size(63, 15)
        Me.KdvPayı.TabIndex = 25
        Me.KdvPayı.Text = "KDV Payı:"
        Me.KdvPayı.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.KdvPayı, "KDV Payı, %18 Olarak Hesaplanmaktadır.")
        '
        'KdvPayıDeğer
        '
        Me.KdvPayıDeğer.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.KdvPayıDeğer.ForeColor = System.Drawing.Color.Red
        Me.KdvPayıDeğer.Location = New System.Drawing.Point(156, 161)
        Me.KdvPayıDeğer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.KdvPayıDeğer.Name = "KdvPayıDeğer"
        Me.KdvPayıDeğer.Size = New System.Drawing.Size(78, 21)
        Me.KdvPayıDeğer.TabIndex = 26
        Me.KdvPayıDeğer.Text = "0"
        Me.KdvPayıDeğer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'KültürPayı
        '
        Me.KültürPayı.AutoSize = True
        Me.KültürPayı.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.KültürPayı.Location = New System.Drawing.Point(158, 38)
        Me.KültürPayı.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.KültürPayı.Name = "KültürPayı"
        Me.KültürPayı.Size = New System.Drawing.Size(74, 15)
        Me.KültürPayı.TabIndex = 27
        Me.KültürPayı.Text = "Kültür Payı:"
        Me.KültürPayı.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.KültürPayı, "Kültür Bakanlığı Payı, %1 Olarak Hesaplanmaktadır.")
        '
        'KültürPayıDeğer
        '
        Me.KültürPayıDeğer.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.KültürPayıDeğer.ForeColor = System.Drawing.Color.Red
        Me.KültürPayıDeğer.Location = New System.Drawing.Point(156, 53)
        Me.KültürPayıDeğer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.KültürPayıDeğer.Name = "KültürPayıDeğer"
        Me.KültürPayıDeğer.Size = New System.Drawing.Size(78, 21)
        Me.KültürPayıDeğer.TabIndex = 28
        Me.KültürPayıDeğer.Text = "0"
        Me.KültürPayıDeğer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SatışFiyatıDeğer
        '
        Me.SatışFiyatıDeğer.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.SatışFiyatıDeğer.ForeColor = System.Drawing.Color.Green
        Me.SatışFiyatıDeğer.Location = New System.Drawing.Point(156, 197)
        Me.SatışFiyatıDeğer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.SatışFiyatıDeğer.Name = "SatışFiyatıDeğer"
        Me.SatışFiyatıDeğer.Size = New System.Drawing.Size(78, 21)
        Me.SatışFiyatıDeğer.TabIndex = 30
        Me.SatışFiyatıDeğer.Text = "0"
        Me.SatışFiyatıDeğer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SatışFiyatı
        '
        Me.SatışFiyatı.AutoSize = True
        Me.SatışFiyatı.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.SatışFiyatı.Location = New System.Drawing.Point(159, 182)
        Me.SatışFiyatı.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.SatışFiyatı.Name = "SatışFiyatı"
        Me.SatışFiyatı.Size = New System.Drawing.Size(72, 15)
        Me.SatışFiyatı.TabIndex = 29
        Me.SatışFiyatı.Text = "Satış Fiyatı:"
        Me.SatışFiyatı.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-2, 202)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(27, 27)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 31
        Me.PictureBox1.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox1, "Bilgileri Ekran Görüntüsünü Olarak Kaydet")
        '
        'HesapTürü
        '
        Me.HesapTürü.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.HesapTürü.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.HesapTürü.FormattingEnabled = True
        Me.HesapTürü.Items.AddRange(New Object() {"İthalat Kayıtlı", "Pasaport Kayıtlı"})
        Me.HesapTürü.Location = New System.Drawing.Point(26, 63)
        Me.HesapTürü.Margin = New System.Windows.Forms.Padding(2)
        Me.HesapTürü.Name = "HesapTürü"
        Me.HesapTürü.Size = New System.Drawing.Size(115, 23)
        Me.HesapTürü.TabIndex = 32
        '
        'ParaBirimi
        '
        Me.ParaBirimi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ParaBirimi.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ParaBirimi.FormattingEnabled = True
        Me.ParaBirimi.Items.AddRange(New Object() {"Dolar", "Euro", "TL"})
        Me.ParaBirimi.Location = New System.Drawing.Point(26, 104)
        Me.ParaBirimi.Margin = New System.Windows.Forms.Padding(2)
        Me.ParaBirimi.Name = "ParaBirimi"
        Me.ParaBirimi.Size = New System.Drawing.Size(115, 23)
        Me.ParaBirimi.TabIndex = 33
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label4.Location = New System.Drawing.Point(26, 45)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 18)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Hesaplama Türü"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label5.Location = New System.Drawing.Point(26, 86)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 18)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Para Birimi"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'KayıtsızPanel
        '
        Me.KayıtsızPanel.Controls.Add(Me.ToplamMaliyet)
        Me.KayıtsızPanel.Controls.Add(Me.Label7)
        Me.KayıtsızPanel.Controls.Add(Me.KayıtÜcretiDeğer)
        Me.KayıtsızPanel.Controls.Add(Me.Label9)
        Me.KayıtsızPanel.Controls.Add(Me.TrtPayıDeğer2)
        Me.KayıtsızPanel.Controls.Add(Me.Label11)
        Me.KayıtsızPanel.Controls.Add(Me.VergisizFiyatDeğer2)
        Me.KayıtsızPanel.Controls.Add(Me.Label13)
        Me.KayıtsızPanel.Location = New System.Drawing.Point(142, 2)
        Me.KayıtsızPanel.Margin = New System.Windows.Forms.Padding(2)
        Me.KayıtsızPanel.Name = "KayıtsızPanel"
        Me.KayıtsızPanel.Size = New System.Drawing.Size(97, 228)
        Me.KayıtsızPanel.TabIndex = 37
        Me.KayıtsızPanel.Visible = False
        '
        'ToplamMaliyet
        '
        Me.ToplamMaliyet.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.ToplamMaliyet.ForeColor = System.Drawing.Color.Green
        Me.ToplamMaliyet.Location = New System.Drawing.Point(8, 197)
        Me.ToplamMaliyet.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.ToplamMaliyet.Name = "ToplamMaliyet"
        Me.ToplamMaliyet.Size = New System.Drawing.Size(78, 21)
        Me.ToplamMaliyet.TabIndex = 46
        Me.ToplamMaliyet.Text = "0"
        Me.ToplamMaliyet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label7.Location = New System.Drawing.Point(-2, 172)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(98, 15)
        Me.Label7.TabIndex = 45
        Me.Label7.Text = "Toplam Maliyet:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'KayıtÜcretiDeğer
        '
        Me.KayıtÜcretiDeğer.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.KayıtÜcretiDeğer.ForeColor = System.Drawing.Color.Red
        Me.KayıtÜcretiDeğer.Location = New System.Drawing.Point(8, 141)
        Me.KayıtÜcretiDeğer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.KayıtÜcretiDeğer.Name = "KayıtÜcretiDeğer"
        Me.KayıtÜcretiDeğer.Size = New System.Drawing.Size(78, 21)
        Me.KayıtÜcretiDeğer.TabIndex = 44
        Me.KayıtÜcretiDeğer.Text = "0"
        Me.KayıtÜcretiDeğer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label9.Location = New System.Drawing.Point(8, 116)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 15)
        Me.Label9.TabIndex = 43
        Me.Label9.Text = "Kayıt Ücreti:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.Label9, "Kayıt Ücreti, Pasaporta İşletme Ücreti yada IMEI Kayıt Ücretidir.")
        '
        'TrtPayıDeğer2
        '
        Me.TrtPayıDeğer2.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.TrtPayıDeğer2.ForeColor = System.Drawing.Color.Red
        Me.TrtPayıDeğer2.Location = New System.Drawing.Point(8, 85)
        Me.TrtPayıDeğer2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TrtPayıDeğer2.Name = "TrtPayıDeğer2"
        Me.TrtPayıDeğer2.Size = New System.Drawing.Size(78, 21)
        Me.TrtPayıDeğer2.TabIndex = 42
        Me.TrtPayıDeğer2.Text = "0"
        Me.TrtPayıDeğer2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label11.Location = New System.Drawing.Point(17, 60)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 15)
        Me.Label11.TabIndex = 41
        Me.Label11.Text = "TRT Payı:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.Label11, "TRT Payı, %10 Olarak Hesaplanmaktadır.")
        '
        'VergisizFiyatDeğer2
        '
        Me.VergisizFiyatDeğer2.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.VergisizFiyatDeğer2.ForeColor = System.Drawing.Color.Red
        Me.VergisizFiyatDeğer2.Location = New System.Drawing.Point(8, 29)
        Me.VergisizFiyatDeğer2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.VergisizFiyatDeğer2.Name = "VergisizFiyatDeğer2"
        Me.VergisizFiyatDeğer2.Size = New System.Drawing.Size(78, 21)
        Me.VergisizFiyatDeğer2.TabIndex = 40
        Me.VergisizFiyatDeğer2.Text = "0"
        Me.VergisizFiyatDeğer2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label13.Location = New System.Drawing.Point(4, 4)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(86, 15)
        Me.Label13.TabIndex = 39
        Me.Label13.Text = "Vergisiz Fiyat:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.Label13, "Vergisiz Fiyat, Vergilerin Dahil Olmadığı Ham Fiyattır.")
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Black
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(-2, 228)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(243, 21)
        Me.Label6.TabIndex = 39
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Window
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(126, 147)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(10, 14)
        Me.Label8.TabIndex = 40
        Me.Label8.Text = "$"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-2, 176)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(26, 26)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 41
        Me.PictureBox2.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox2, "Bilgileri Yazı Olarak Panoya Kopyala")
        '
        'KurYenile
        '
        Me.KurYenile.Image = CType(resources.GetObject("KurYenile.Image"), System.Drawing.Image)
        Me.KurYenile.Location = New System.Drawing.Point(6, 24)
        Me.KurYenile.Margin = New System.Windows.Forms.Padding(2)
        Me.KurYenile.Name = "KurYenile"
        Me.KurYenile.Size = New System.Drawing.Size(15, 15)
        Me.KurYenile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.KurYenile.TabIndex = 42
        Me.KurYenile.TabStop = False
        Me.ToolTip1.SetToolTip(Me.KurYenile, "Kur Bilgilerini Güncelle")
        '
        'ToolTip1
        '
        Me.ToolTip1.ToolTipTitle = "İPUCU"
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(-2, 150)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(26, 26)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 43
        Me.PictureBox3.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox3, "Bilgileri Resim Olarak Panoya Kopyala")
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(240, 227)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.KurYenile)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.KayıtsızPanel)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ParaBirimi)
        Me.Controls.Add(Me.HesapTürü)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.SatışFiyatıDeğer)
        Me.Controls.Add(Me.SatışFiyatı)
        Me.Controls.Add(Me.KültürPayıDeğer)
        Me.Controls.Add(Me.KültürPayı)
        Me.Controls.Add(Me.KdvPayıDeğer)
        Me.Controls.Add(Me.KdvPayı)
        Me.Controls.Add(Me.ÖtvPayıDeğer)
        Me.Controls.Add(Me.ÖtvPayı)
        Me.Controls.Add(Me.TrtPayıDeğer)
        Me.Controls.Add(Me.TrtPayı)
        Me.Controls.Add(Me.VergisizFiyatDeğer)
        Me.Controls.Add(Me.VergisizFiyat)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Hesapla)
        Me.Controls.Add(Me.Kur)
        Me.Controls.Add(Me.Fiyat)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Telefon Fiyatı Hesapla v2"
        CType(Me.Kur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.KayıtsızPanel.ResumeLayout(False)
        Me.KayıtsızPanel.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KurYenile, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As Label
    Friend WithEvents Hesapla As Button
    Friend WithEvents Kur As NumericUpDown
    Friend WithEvents Fiyat As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents VergisizFiyat As Label
    Friend WithEvents VergisizFiyatDeğer As Label
    Friend WithEvents TrtPayı As Label
    Friend WithEvents TrtPayıDeğer As Label
    Friend WithEvents ÖtvPayı As Label
    Friend WithEvents ÖtvPayıDeğer As Label
    Friend WithEvents KdvPayı As Label
    Friend WithEvents KdvPayıDeğer As Label
    Friend WithEvents KültürPayı As Label
    Friend WithEvents KültürPayıDeğer As Label
    Friend WithEvents SatışFiyatıDeğer As Label
    Friend WithEvents SatışFiyatı As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents HesapTürü As ComboBox
    Friend WithEvents ParaBirimi As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents KayıtsızPanel As Panel
    Friend WithEvents ToplamMaliyet As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents KayıtÜcretiDeğer As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents TrtPayıDeğer2 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents VergisizFiyatDeğer2 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents KurYenile As PictureBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents PictureBox3 As PictureBox
End Class
