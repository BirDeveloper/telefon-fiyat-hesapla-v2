﻿Public Class Form1
    Dim vergisiz, trt, ötv, kdv, kültür, değişkenötv, toplam As Integer
    Dim vergisiz2, trt2, kayıtücreti, toplam2 As Integer
    Dim dolaroku, eurooku, manueleurokuru2 As String

    Private Sub Kur_ValueChanged(sender As Object, e As EventArgs) Handles Kur.ValueChanged
        Kur.Select(6, 0)
        Kur.Value = Math.Floor(100 * Kur.Value) / 100
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        Dim telefonmodeli As String = InputBox("Lütfen Telefon Marka ve Modelini Girin" & vbNewLine & vbNewLine & "Örn: Iphone SE" & vbNewLine & vbNewLine & "(Boş Bırakarak Atlayabilirsiniz)", "Lütfen Telefon Marka ve Modelini Girin!", "")
        If telefonmodeli <> "" Then
            telefonmodeli = "Telefon: " & telefonmodeli & vbNewLine
        Else
            telefonmodeli = ""
        End If
        If HesapTürü.SelectedIndex = 0 Then
            My.Computer.Clipboard.SetText(telefonmodeli & VergisizFiyat.Text & " " & VergisizFiyatDeğer.Text & vbNewLine & "Kültür Bakanlığı Payı (%1):" & " " & KültürPayıDeğer.Text & vbNewLine & "TRT Bandrol Ücreti (%10):" & " " & TrtPayıDeğer.Text & vbNewLine & "ÖTV Payı (%" & değişkenötv & "): " & ÖtvPayıDeğer.Text & vbNewLine & "KDV Payı (%18):" & " " & KdvPayıDeğer.Text & vbNewLine & SatışFiyatı.Text & " " & SatışFiyatıDeğer.Text)
        ElseIf HesapTürü.SelectedIndex = 1 Then
            My.Computer.Clipboard.SetText(telefonmodeli & Label13.Text & " " & VergisizFiyatDeğer2.Text & vbNewLine & "TRT Bandrol Ücreti (20 Euro): " & TrtPayıDeğer2.Text & vbNewLine & "IMEI Kayıt Ücreti: 1838₺" & vbNewLine & Label7.Text & " " & ToplamMaliyet.Text)
        End If
    End Sub

    Private Sub KurYenile_Click(sender As Object, e As EventArgs) Handles KurYenile.Click
        Kur.Value = Kur.Value

        If dolaroku <> Nothing Then
            If ParaBirimi.SelectedIndex = 0 Then
                kurgüncelle()
                Kur.Value = Val(dolaroku)
            ElseIf ParaBirimi.SelectedIndex = 1 Then
                kurgüncelle()
                Kur.Value = Val(eurooku)
            End If
        Else
            If ParaBirimi.SelectedIndex = 0 Then
                Kur.Value = "6,90"
            ElseIf ParaBirimi.SelectedIndex = 1 Then
                Kur.Value = "7,50"
            End If
        End If


    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Dim telefonmodeli As String = InputBox("Lütfen Ekran Görüntüsünde Yazmasını İstediğiniz Telefonun Marka/Modelini Yazınız." & vbNewLine & vbNewLine & "Örn: Iphone SE" & vbNewLine & vbNewLine & "(Boş Bırakarak Atlayabilirsiniz)", "Lütfen Telefonun Marka ve Modelini Girin!", "")
        If telefonmodeli <> "" Then
            MyBase.Height = ("345")
            Label6.Text = telefonmodeli
        End If
        Dim format As Imaging.ImageFormat = Imaging.ImageFormat.Png
        Dim image = New Bitmap(Me.Width - 6, Me.Height - 5)
        Using g As Graphics = Graphics.FromImage(image)
            Me.ControlBox = False
            KurYenile.Visible = False
            PictureBox1.Visible = False
            PictureBox2.Visible = False
            PictureBox3.Visible = False
            bekle(0.2)
            g.CopyFromScreen(Me.Location, New Point(-3, -2), Me.Size)
            Me.ControlBox = True
            KurYenile.Visible = True
            PictureBox1.Visible = True
            PictureBox2.Visible = True
            PictureBox3.Visible = True
        End Using
        My.Computer.Clipboard.SetImage(image)
        Me.Height = "319"
        Label6.Text = ""
    End Sub

    Public Sub bekle(ByVal saniye As Single)
        Static başlat As Single
        başlat = Microsoft.VisualBasic.Timer()
        Do While Microsoft.VisualBasic.Timer() < başlat + saniye
            System.Windows.Forms.Application.DoEvents()
        Loop
    End Sub
    Public Sub Kaydet(frm As Form)
        Dim telefonmodeli As String = InputBox("Lütfen Ekran Görüntüsünde Yazmasını İstediğiniz Telefonun Marka/Modelini Yazınız." & vbNewLine & vbNewLine & "Örn: Iphone SE" & vbNewLine & vbNewLine & "(Boş Bırakarak Atlayabilirsiniz)", "Lütfen Telefonun Marka ve Modelini Girin!", "")
        If telefonmodeli <> "" Then
            MyBase.Height = ("345")
            Label6.Text = telefonmodeli
        End If
        Dim format As Imaging.ImageFormat = Imaging.ImageFormat.Png
        Dim image = New Bitmap(frm.Width - 6, frm.Height - 5)
        Using g As Graphics = Graphics.FromImage(image)
            Me.ControlBox = False
            KurYenile.Visible = False
            PictureBox1.Visible = False
            PictureBox2.Visible = False
            PictureBox3.Visible = False
            bekle(0.2)
            g.CopyFromScreen(frm.Location, New Point(-3, -2), frm.Size)
            Me.ControlBox = True
            KurYenile.Visible = True
            PictureBox1.Visible = True
            PictureBox2.Visible = True
            PictureBox3.Visible = True
            Me.Height = "319"
        End Using
        Using sfd As New SaveFileDialog
            sfd.Filter = "PNG Dosyası(*.png)|*.png"
            If sfd.ShowDialog = DialogResult.OK Then
                Label6.Text = ""
                Try
                    image.Save(sfd.FileName, format)
                Catch ex As Exception
                    MessageBox.Show("Dosya adı: " & sfd.FileName & vbCrLf & vbCrLf & ex.Message, "Hata!")
                End Try
            End If
        End Using
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Kaydet(Me)
    End Sub

    Private Sub ParaBirimi_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ParaBirimi.SelectedIndexChanged
        If ParaBirimi.SelectedIndex = 0 Then
            Label1.Text = "Dolar Kuru"
            Kur.Value = "6,90"
            If dolaroku <> Nothing Then
                Kur.Value = CDec(Val(dolaroku))
            End If
            Label1.Visible = True
            Kur.Visible = True
            KurYenile.Visible = True
            Label8.Text = "$"
        ElseIf ParaBirimi.SelectedIndex = 1 Then
            Label1.Text = "Euro Kuru"
            Kur.Value = "7,50"
            If eurooku <> Nothing Then
                Kur.Value = CDec(Val(eurooku))
            End If
            Label1.Visible = True
            Kur.Visible = True
            KurYenile.Visible = True
            Label8.Text = "€"
        ElseIf ParaBirimi.SelectedIndex = 2 Then
            Kur.Value = "1"
            Label1.Visible = False
            Kur.Visible = False
            KurYenile.Visible = False
            Label8.Text = "₺"
        End If
    End Sub

    Private Sub HesapTürü_SelectedIndexChanged(sender As Object, e As EventArgs) Handles HesapTürü.SelectedIndexChanged
        If HesapTürü.SelectedIndex = 0 Then
            KayıtsızPanel.Hide()
        ElseIf HesapTürü.SelectedIndex = 1 Then
            KayıtsızPanel.Show()
        End If
    End Sub
    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        kurgüncelle()
        If dolaroku <> Nothing Then
            Kur.Value = Val(dolaroku)
        End If
        HesapTürü.SelectedIndex = 0
        ParaBirimi.SelectedIndex = 0
    End Sub

    Private Sub Fiyat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Fiyat.KeyPress
        If (e.KeyChar < "0" OrElse e.KeyChar > "9") _
           AndAlso e.KeyChar <> ControlChars.Back AndAlso e.KeyChar <> "." AndAlso e.KeyChar <> "," Then
            e.Handled = True
        End If
    End Sub
    Private Sub Hesapla_Click(sender As Object, e As EventArgs) Handles Hesapla.Click
        If HesapTürü.SelectedIndex = 0 Then
            vergisiz = "0" And trt = "0" And ötv = "0" And kdv = "0" And kültür = "0" And toplam = "0"
            vergisiz = Val(Fiyat.Text) * Val(Kur.Value)
            VergisizFiyatDeğer.Text = vergisiz & " ₺"
            If Val(vergisiz > 1500) Then
                değişkenötv = "50"
            ElseIf Val(vergisiz > 640) And Val(vergisiz <= 1500) Then
                değişkenötv = "40"
            ElseIf Val(vergisiz <= 640) Then
                değişkenötv = "25"
            End If
            kültür = (Val(vergisiz) / 100)
            KültürPayıDeğer.Text = kültür & " ₺"
            trt = Val(vergisiz / 100 * 10)
            TrtPayıDeğer.Text = trt & " ₺"
            ötv = Val(vergisiz + trt + kültür) / 100 * değişkenötv
            ÖtvPayıDeğer.Text = ötv & " ₺"
            kdv = (Val(vergisiz) + Val(kültür) + Val(trt) + Val(ötv)) / 100 * 18
            KdvPayıDeğer.Text = kdv & " ₺"
            toplam = Val(vergisiz) + Val(trt) + Val(ötv) + Val(kdv) + Val(kültür)
            SatışFiyatıDeğer.Text = toplam & " ₺"
        ElseIf HesapTürü.SelectedIndex = 1 Then
            vergisiz2 = "0" And trt2 = "0" And kayıtücreti = "0" And toplam2 = "0"
            vergisiz2 = Val(Fiyat.Text) * Val(Kur.Value)
            VergisizFiyatDeğer2.Text = vergisiz2 & " ₺"
            If eurooku <> Nothing Then
                If ParaBirimi.SelectedIndex = 1 Then
                    trt2 = Val(20) * Val(Kur.Value)
                Else
                    trt2 = Val(20) * Val(eurooku)
                End If
            Else
                Try
                    Dim manueleurokuru As String = InputBox("Lütfen Güncel Euro Kurunu Girin" & vbNewLine & vbNewLine & "Örn: 7,50", "Lütfen Güncel Euro Kurunu Girin!", manueleurokuru2)
                    If manueleurokuru <> "" Then
                        manueleurokuru2 = manueleurokuru
                        manueleurokuru = Convert.ToDecimal(manueleurokuru.ToString.Replace(".", ","))
                    ElseIf TrtPayıDeğer2.Text = "0" Then
                        MsgBox("Euro Kuru Girmediğiniz İçin Trt Payı Hesaplanamadı!")
                    End If
                    trt2 = Val("20") * CDec(manueleurokuru)
                Catch ex As Exception
                End Try
            End If
            TrtPayıDeğer2.Text = trt2 & " ₺"
            kayıtücreti = "1838"
            KayıtÜcretiDeğer.Text = kayıtücreti & " ₺"
            toplam2 = Val(vergisiz2 + trt2 + kayıtücreti)
            ToplamMaliyet.Text = toplam2 & " ₺"
        End If
    End Sub
    Sub kurgüncelle()
        Try
            Dim request As Net.HttpWebRequest
            Dim response As Net.HttpWebResponse = Nothing
            Dim reader As IO.StreamReader
            Net.ServicePointManager.SecurityProtocol = CType(3072, Net.SecurityProtocolType)
            request = DirectCast(Net.WebRequest.Create("https://api.bigpara.hurriyet.com.tr/doviz/headerlist/anasayfa"), Net.HttpWebRequest)
            response = DirectCast(request.GetResponse(), Net.HttpWebResponse)
            reader = New IO.StreamReader(response.GetResponseStream())

            Dim read = Newtonsoft.Json.Linq.JObject.Parse(reader.ReadToEnd)
            dolaroku = read("data")(6)("ALIS")
            eurooku = read("data")(3)("ALIS")
            dolaroku = dolaroku.Substring(0, dolaroku.IndexOf(".") + 3)
            eurooku = eurooku.Substring(0, dolaroku.IndexOf(".") + 3)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Kur_TextChanged(sender As Object, e As EventArgs) Handles Kur.TextChanged
        Kur.Select(6, 0)
    End Sub
End Class